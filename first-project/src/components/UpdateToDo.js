import React, { Component } from 'react'
import { connect } from "react-redux"

export class UpdateToDo extends Component {
    handleUpdate = event => {
        event.preventDefault();
        const updatedItem = this.item.value;
        const data = {
            updatedItem
        }
        console.log("Hello");
        console.log(data);
        this.props.dispatch({ type: "UPDATE_TODO", id: this.props.todo.id, data: data })
    }
    render() {
        return (
            <div>
                <center>
                    <form action="" onSubmit={this.handleUpdate}>
                        <input required placeholder="Enter Your To do item" type="text" ref={(input) => this.item = input} defaultValue={this.props.todo.item} />

                        <button type="submit">Update</button>



                    </form>


                </center>


            </div>
        )
    }
}

export default connect()(UpdateToDo)
