import React, { Component } from 'react'
import { connect } from "react-redux"

export class ToDo extends Component {
    render() {
        return (
            <div>
                <center>
                    <table>
                        <tr>
                            <td>
                                {this.props.todo.item}
                            </td>
                            <td>
                                {<button onClick={() => this.props.dispatch(
                                    { type: "EDIT_TODO", id: this.props.todo.id }
                                )}>Edit</button>}
                            </td>

                            <td>
                                {<button onClick={() => this.props.dispatch({ type: "DELETE_TODO", id: this.props.todo.id })}>Delete</button>}
                            </td>


                        </tr>

                    </table>



                </center>


            </div>
        )
    }
}

export default connect()(ToDo);
