// import React, { Component } from 'react'
// import { connect } from "react-redux";
// import { bindActionCreators } from "redux"
// import { AddToDo } from "../actions"

// const CreateToDo = ({ dispatch }) => {
//     let input
//     return (
//         <div>
//             <center>
//                 <div>
//                     <h3> My To do Application</h3>

//                 </div>
//                 <form onSubmit={e => {
//                     e.preventDefault()
//                     if (!input.value.trim()) {
//                         return
//                     }
//                     dispatch(AddToDo(input.value))
//                     input.value = ''
//                 }}>
//                     <input ref={node => input = node} />
//                     <button type="submit">
//                         Add Todo
//               </button>
//                 </form>
//             </center>
//         </div>





//     );


// }
// export default connect()(CreateToDo);



import React, { Component } from 'react'
import { connect } from "react-redux"


class CreateToDo extends Component {
    handleSubmit = event => {
        event.preventDefault();
        const item = this.item.value;
        const data = {
            id: new Date(),
            item,
            editing: false
        }
        //console.log(data);
        this.props.dispatch({
            type: "ADD_TODO",
            data
        });
        this.item.value = ""
    }
    render() {
        return (
            <div>
                <center>
                    <h3>Add Your To Do </h3>
                    <form action="" onSubmit={this.handleSubmit}>
                        <input required placeholder="Enter Your To do item" type="text" ref={(input) => this.item = input} />

                        <button type="submit">Add item</button>


                    </form>
                </center>

            </div>
        )
    }
}

export default connect()(CreateToDo);
