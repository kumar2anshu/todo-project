
import React, { Component } from 'react'
import { connect } from "react-redux";
import ToDo from "./ToDo"
import UpdateToDo from "./UpdateToDo"



class ToDoList extends Component {
    render() {
        return (
            <div>
                <center>
                    <table>
                        <tr>
                            <th>
                                To Do List
                            </th>
                            <th>
                                Action 1
                            </th>
                            <th>
                                Action 2
                            </th>
                        </tr>
                    </table>


                </center>

                {/*{console.log(this.props.todos)} */}

                {this.props.todos.map((todo) => (
                    <div key={todo.id}>
                        {
                            todo.editing ? <UpdateToDo key={todo.id} todo={todo} /> :
                                <ToDo key={todo.id} todo={todo} />
                        }

                    </div>
                ))}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        todos: state
    }
}
export default connect(mapStateToProps)(ToDoList);


