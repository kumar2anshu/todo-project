
const reducerFunction = (state = [], action) => {

    switch (action.type) {
        case "ADD_TODO":
            return state.concat([action.data]);

        case "EDIT_TODO":

            return state.map((todo) => todo.id === action.id ? { ...todo, editing: !todo.editing } : todo)

        case "UPDATE_TODO":
            return state.map((todo) => {
                if (todo.id === action.id) {
                    return {
                        ...todo,
                        item: action.data.updatedItem,
                        editing: !todo.editing


                    }
                }
                else return todo;
            })


        case "DELETE_TODO":
            // = state.slice();
            //state.splice(action.payload, 1);
            // break;
            return state.filter((todo) => todo.id !== action.id)
        default:
            return state;


    }
}


export default reducerFunction;