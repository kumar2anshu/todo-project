import React from 'react';
import './App.css';
import CreateToDo from './components/CreateToDo';
import ToDoList from './components/ToDoList';


const App = () => {
  return (
    <div>
      <CreateToDo />
      <ToDoList />

    </div>

  );
}

export default App;
